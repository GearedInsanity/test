<!DOCTYPE html>
<html lang="en">
<head>
  <title>Product list</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <div class="row" style="margin-top:50px;">
    <div class="col-sm-2">
      <h3>Product List</h3>
    </div>

    <div class="col-sm-8"> <!-- For the empty space between these two -->
    </div>

    <div class="col-sm-2" style="text-align:right; display:inline-block;">
      <form action="productAdd.php">
        <input type="submit" value="Add" />
      </form>
      <!--<form action="productAdd.php">
        <input type="submit" name="delete" id="delete" value="Mass Delete" /> <!-- Sis viss rada delete pogas logiku... -->
      <!--</form> -->
    </div>

  </div>
</div>

<hr size="1" width="90%" color="black"> <!-- The color and size does nothig... The only responsive thing for this line is "width"-->

<div class="container">
  <form method="POST" action="objectDelete.php">
    <input type="submit" name="deleteMultipleBtn" value="Delete" />
    <!--<input type="submit" name="delete" id="delete" value="Mass Delete" /> -->
  <?php require_once 'process.php'; ?>

  <?php
    $mysqli = new mysqli('localhost', 'id16913831_root', 'Youguysaregay5@', 'id16913831_items') or die(mysql_error($mysqli)); // This is how you connect to a MYSQL database!
    $dvdSql = $mysqli->query("SELECT * FROM dvd") or die($mysqli->error); // Connection to the DVD database
    $bookSql = $mysqli->query("SELECT * FROM book") or die($mysqli->error); // Connection to the BOOk database
    $furnSql = $mysqli->query("SELECT * FROM furniture") or die($mysqli->error); // Connection to the FURNITURE database
    //pre_r($result);
    //pre_r($result->fetch_assoc());
  ?>

  <div class ="row justify-content-center">
    <!-- <table class = "table"> -->
    <!--<div class="card" style="width: 18rem;">
      <div class="card-body" style="text-align:center">
        <h5 class="card-title">Card title</h5>
        <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" class="card-link">Card link</a>
        <a href="#" class="card-link">Another link</a>
      </div>
    </div>

    <div class="card" style="width: 18rem;">
      <div class="card-body" style="text-align:center">
        <h5 class="card-title">Card title</h5>
        <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" class="card-link">Card link</a>
        <a href="#" class="card-link">Another link</a>
      </div>

    </div> -->


      <?php
        while($row = $dvdSql->fetch_assoc()):
          //echo "<tr><td>", $row['sku'] ,"</td><td>", $row['name'] ,"</td><td>", $row['price'],"</td><td>", $row['size'],"</td></tr>"; // This is just here so i dont fuckinf forget what i was doing...
          ?>

          <div class="card" style="width: 14rem; padding:10px; margin:10px;">
            <div class="card-body" style="text-align:center">
              <input type="checkbox" name="records[]" value="<?php echo $row['sku']; ?>">
              <!--<input type="checkbox" id="DelBoxDVD" name="DelBoxDVD" value="DelBoxDVD" style="text-align: left;"> -->
              <h5 class="card-subtitle mb-2 text-muted"><?php echo $row['sku']; ?></h5>
              <h4 class="card-title"><?php echo $row['name']; ?></h4>
              <p class="card-text"><?php echo $row['price']; ?>.00 $</p>
              <p href="#" class="card-link">Size: <?php echo $row['size']; ?> MB</p>
            </div>
          </div>

          <?php endwhile; ?>

        <?php
          while($row = $bookSql->fetch_assoc()): // This is just here so i dont fuckinf forget what i was doing...
            ?>

            <div class="card" style="width: 14rem; padding:10px; margin:10px;">
              <div class="card-body" style="text-align:center">
                <input type="checkbox" name="records[]" value="<?php echo $row['sku']; ?>">
                <!--<input type="checkbox" id="DelBoxBOOK" name="DelBoxBOOK" value="DelBoxBOOK" style="text-align: left;"> -->
                <h5 class="card-subtitle mb-2 text-muted"><?php echo $row['sku']; ?></h5>
                <h4 class="card-title"><?php echo $row['name']; ?></h4>
                <p class="card-text"><?php echo $row['price']; ?>.00 $</p>
                <p href="#" class="card-link">Weight: <?php echo $row['weight']; ?> KG</p>
              </div>
            </div>

              <?php endwhile; ?>

          <?php
            while($row = $furnSql->fetch_assoc()): // This is just here so i dont fuckinf forget what i was doing...
              ?>

              <div class="card" style="width: 14rem; padding:10px; margin:10px;">
                <div class="card-body" style="text-align:center">
                  <input type="checkbox" name="records[]" value="<?php echo $row['sku']; ?>">
                  <!--<input type="checkbox" id="DelBoxFUR" name="DelBoxFUR" value="DelBoxFUR" style="text-align: left;"> -->
                  <h5 class="card-subtitle mb-2 text-muted"><?php echo $row['sku']; ?></h5>
                  <h4 class="card-title"><?php echo $row['name']; ?></h4>
                  <p class="card-text"><?php echo $row['price']; ?>.00 $</p>
                  <p href="#" class="card-link">Dimensions: <?php echo $row['height']; ?>x<?php echo $row['width']; ?>x<?php echo $row['length']; ?></p>
                </div>
              </div>

                <?php endwhile; ?>

          <!-- I HAVE NO CLUE WHY THIS PART IS NOT WORKING CORECTLY >:(
          <tr>
            <td><<?php //echo $row['name']; ?></td> I think its because before the php thing i had added another "<" ... Yeah
            <td><<?php //echo $row['location']; ?></td>
            <td></td>
          </tr>
          -->
        <?php //endwhile; ?>

      <!-- </table> -->
    </div>

  <?php

    function pre_r($array){
      echo '<pre>';
      print_r($array);
      echo '</pre>';
    }
   ?>

   <!--
  <div class="row justify-content-center">
    <form action="process.php" method="POST"> <!-- this form is beeing created like this so laitr on I could modify it-->
    <!--  <div class="form-group">
        <label>Name</label>
        <input type="text" name="name" class="form-control" value="Enter your name">
      </div>
      <div class="form-group">
        <label>Location</label>
        <input type="text" name="location" class="form-control" value="Enter your location">
      </div>
      <div class="form-group"> <!-- style was not added because it will be deleted laiter on anyway-->
      <!--  <button type="submit" name="save"> Save </button>
      </div>
    </form>
  </div>
  -->
</form>
</div>

<hr size="1" width="90%" color="black">
<p style="text-align:center;">Scandiweb Test assignemt</p>

</body>
</html>
