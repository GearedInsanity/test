<!DOCTYPE html>
<html lang="en">
<head>
  <title>Product Add</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <script>

  function checkforblank(){
    var Empty = 0;
    // Basic fields
    if(document.getElementById('skuField').value == "" || document.getElementById('nameField').value == "" || document.getElementById('priceField').value == ""){
        Empty++;
    }

    var y = document.getElementById("SelectedType").selectedIndex;
    if(document.getElementById('sizeField').value == "" && y == 0){
        Empty++;
    }
    else if(document.getElementById('weightField').value == "" && y == 1){
        Empty++;
    }
    else if((document.getElementById('heightField').value == "" || document.getElementById('widthField').value == "" || document.getElementById('lengthField').value == "") && y == 2){
        Empty++;
    }


    const skuVal = document.getElementById('skuField').value; // Tutorial has no .value...
    const nameVal = document.getElementById('nameField').value; // Tutorial has no .value...
    const priceVal = document.getElementById('priceField').value; // Tutorial has no .value...
    const weightVal = document.getElementById('priceField').value; // Tutorial has no .value...
    var proceed = 0;

    if(skuVal[0] == 'd' && skuVal[1] == 'i' && skuVal[2] == 's' && skuVal[3] == 'c' && skuVal[4] == '-' && y == 0)
    {
      proceed = 1;
      //alert('A disc is beeing inputed');
    }
    else if(skuVal[0] == 'b' && skuVal[1] == 'o' && skuVal[2] == 'o' && skuVal[3] == 'k' && skuVal[4] == '-' && y == 1)
    {
      proceed = 1;
      //alert('A book is beeing inputed');
    }
    else if(skuVal[0] == 'F' && skuVal[1] == 'u' && skuVal[2] == 'r' && skuVal[3] == '-' && y == 2)
    {
      proceed = 1;
      //alert('A furniture pice is beeing inputed');
    }
    else {
      alert('Invalid SKU field for the selected item type');
    }


    if(Empty > 0)
    {
      alert('Please, submit required data');
      //alert(Empty);
      return false;
    }





  }

  </script>

</head>
<body>

<div class="container">
  <div class="row" style="margin-top:50px;">
    <div class="col-sm-2">
      <h3>Product Add</h3>
    </div>

    <div class="col-sm-8"> <!-- For the empty space between these two -->
    </div>

    <div class="col-sm-2" style="text-align:right; display:inline-block;">
      <!--<form action="process.php"> So far I cant get this svae buton to work
        <input type="submit" value="Save" />
      </form> -->
      <form action="index.php"> <!-- This one might actually just be this and thats it-->
        <input type="submit" value="Cancel" />
      </form>
    </div>

  </div>
</div>

<hr size="1" width="90%" color="black"> <!-- The color and size does nothig... The only responsive thing for this line is "width"-->

<!-- Velviens nederigs skripts...
<script>
function changeStatus()
{
  var status = document.getElementById("SelectedType")
  if(status.value=="1")
  {
    document.getElementById("opt1") style.visibility="hidden";
  }
}
</script> -->

<div class="container">
  <div class="row justify-content-center">
    <form action="process.php" method="POST" onsubmit="return checkforblank()" id="formID"> <!-- return is beeing used here to block the form if it's fields are empty-->
      <div class="form-group">
        <label>SKU</label>
        <input type="text" name="sku" class="form-control" id="skuField" value="">
      </div>
      <div class="form-group">
        <label>Name</label>
        <input type="text" name="name" class="form-control" id="nameField" value="">
      </div>
      <div class="form-group">
        <label>Price ($)</label>
        <input type="number" name="price" class="form-control" id="priceField" value="">
      </div>

      <!-- Well... Here we start having a few problems... -->
      <div class="form-group">
        <div class="menu">
          <label>Type switcher</label>
          <select name="cases" class="form-control" id="SelectedType" onchange="changeStatus()">
            <option id="c1" value="0">DVD-disc</option>
            <option id="c2" value="1">Book</option>
            <option id="c3" value="2">Furniture</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <div class="optional" id="opt1" style="padding:25px; border: 2px solid #8bc34a; border-radius: 8px;">
          <p>Please, provide size</p>
          <label id="opt1">Size (MB)</label>
          <input type="number" name="size" class="form-control" id="sizeField" value="">
        </div>
        <div class="optional" id="opt2" style="padding:25px; border: 2px solid #8bc34a; border-radius: 8px;">
          <div class="form-group">
            <p>Please, provide dimensions</p>
            <label>Height (CM)</label>
            <input type="number" name="heigh" class="form-control" id="heightField" value="">
          </div>
          <div class="form-group">
            <label>Width (CM)</label>
            <input type="number" name="width" class="form-control" id="widthField" value="">
          </div>
          <div class="form-group">
            <label>Length (CM)</label>
            <input type="number" name="length" class="form-control" id="lengthField" value="">
          </div>
        </div>
        <div class="optional" id="opt3" style="padding:25px; border: 2px solid #8bc34a; border-radius: 8px;">
          <p>Please, provide weight</p>
          <label>Weight (KG)</label>
          <input type="number" name="weight" class="form-control" id="weightField" value="">
        </div>
      </div>
      <div class="form-group"> <!-- style was not added because it will be deleted laiter on anyway-->
        <button type="submit" name="save"> Save </button>
      </div>

      <p id="test"></p> <!-- Just to see if inputing elements works... -->

      <?php
      //  $SelectedThingy = document.getElementById("SelectedType").selectedIndex;
      // does not seem to work like i intended to.
      ?>

      <script>

      // use display not visability because display makes the object completely disapear or appear where visability just makes it not visable but still makes the object be there.
      // By default everything should not bee visable
      document.getElementById("opt1").style.display = "block"; // this one is vissable because DvD is the default value...
      document.getElementById("opt2").style.display = "none";
      document.getElementById("opt3").style.display = "none";

      function changeStatus(){
        var x = document.getElementById("SelectedType").selectedIndex; // Example of selectedIndex
        if(x == 0){
          //document.getElementById("test").innerHTML = "cat"; // USE THIS TO MAKE THE DESCRIPTIONS TO SHOW UP FOR OBJECTS!
          document.getElementById("opt1").style.display = "block";
          document.getElementById("opt2").style.display = "none";
          document.getElementById("opt3").style.display = "none";
        }
        else if(x == 2){
          //document.getElementById("test").innerHTML = "dog";
          document.getElementById("opt1").style.display = "none";
          document.getElementById("opt2").style.display = "block";
          document.getElementById("opt3").style.display = "none";
        }
        else if(x == 1){
          //document.getElementById("test").innerHTML = "bird";
          document.getElementById("opt1").style.display = "none";
          document.getElementById("opt2").style.display = "none";
          document.getElementById("opt3").style.display = "block";
        }
      }

      </script>

      <!--
      //if(document.getElementById("SelectedType").selectedIndex == 0){
      //  document.getElementById("opt1").style.visibility = "hidden";
      //}
      //document.getElementById("opt1").style.visibility = "hidden";
    //document.getElementById("opt2").style.visibility = "hidden";
      //document.getElementById("opt3").style.visibility = "hidden";

<!--
      <script>
      $("#cases").change(function () {
           var case = $( "#cases option:selected" ).val();
           if(case=="general")
           {
             //show 2 form fields here and show div
              $("#general").hide();
           }
       });
      </script>


   <script>
      $("#field").change(function(){
        if ($(this).val() == "1") {
          $('#otherFieldDiv').show();
          $('#otherField').attr('required', '');
          $('#otherField').attr('data-error', 'This field is required.');
        } else {
          $('#otherFieldDiv').hide();
          $('#otherField').removeAttr('required');
          $('#otherField').removeAttr('data-error');
        }
      });
      </script>

     $("#seeAnotherField").change(function() {
  if ($(this).val() == "yes") {
    $('#otherFieldDiv').show();
    $('#otherField').attr('required', '');
    $('#otherField').attr('data-error', 'This field is required.');
  } else {
    $('#otherFieldDiv').hide();
    $('#otherField').removeAttr('required');
    $('#otherField').removeAttr('data-error');
  }
});
$("#seeAnotherField").trigger("change");


      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script>

        $(document).ready(function(){
          $("#name").on('change', function(){
            $(".optional").hide();
            $("#" + $(this).val()).fadein(700);
          }).change();
        })

      </script>
-->
    </form>
  </div>
</div>

<hr size="1" width="90%" color="black">
<p style="text-align:center;">Scandiweb Test assignemt</p>

</body>
</html>
